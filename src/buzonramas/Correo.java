/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package buzonramas;

/**
 *
 * @author ebarreirosuris
 */
public class Correo {
    
    private String contenido;
    private boolean leido;
    
    
    public Correo(){
        contenido="";
        leido=false;
    }
    public Correo(String texto){
        
        contenido=texto;
        leido=false;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public boolean isLeido() {
        return leido;
    }

    public void setLeido(boolean leido) {
        this.leido = leido;
    }
}
