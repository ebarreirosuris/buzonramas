/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package buzonramas;

import java.util.ArrayList;

/**
 *
 * @author ebarreirosuris
 */
public class Buzon {
    private ArrayList<Correo> alCorreo;
    
    public Buzon(){
        
        alCorreo=new ArrayList();
    }
    //Hector hace los 3 primeros metodos
    public int numCorreos(){
        
        return alCorreo.size();
    }
    
    public void engadeC(String texto){
        
        Correo c1=new Correo(texto);
        alCorreo.add(c1);
    }
    
    public boolean porLer(){
        
        for(Correo c1:alCorreo){
            
            if(!c1.isLeido()){
                return true;
            }
        }
        return false;
    }
    
    // Otros 3 para elias
    public String amosarPrimerNoLeido(){
        
        for(Correo c1:alCorreo){
            
            if(!c1.isLeido()){
                
                c1.setLeido(true);
                return c1.getContenido();
            }
        }
        return "No quedan correos por leer";
    }
    
    public String amosar(int n){
        
        alCorreo.get(n).setLeido(true);
        return alCorreo.get(n).getContenido();
    }
    
    public void elimina(int n){
        
        this.alCorreo.remove(n);
    }
}
