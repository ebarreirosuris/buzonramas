/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package buzonramas;

import java.util.Scanner;

/**
 *
 * @author ebarreirosuris
 */
public class BuzonRamas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int opcion;
        Buzon b1=new Buzon();
        Buzon b2=new Buzon();
        Scanner sc=new Scanner(System.in);
        
        b1.engadeC("Mensaje1: Hola hoy no voy a cenar");
        b1.engadeC("Mensaje2: Hola, al final si voy a cenar");
        b1.engadeC("Mensaje3: Ahora no me apetece cenar, cenas sola");
        
        System.out.println("Tiene usted "+b1.numCorreos()+" mensajes en total.");
        if(b1.porLer()){
            System.out.println("Tiene usted mensajes por leer.");
        }else{
            System.out.println("No tiene mensajes sin leer.");
        }
        do{
            System.out.println("\n----------------------------------");
            System.out.println( "\n1- Mostrar primer mensaje no leido."
                    + "\n2- Mostrar un mensaje determinado."
                    + "\n3- Borrar un mensaje determinado."
                    +"\n4- Salir."
                    +"\nPor favor elija una opcion: ");
            opcion=sc.nextInt();
        
            switch(opcion){
            
                case 1: System.out.println("**"+b1.amosarPrimerNoLeido()+"**1");
                        break;
                            
                case 2: System.out.println("Indique el numero del mensaje que desea mostrar: ");
                        int nMostrar=sc.nextInt()-1;
                        System.out.println("**"+b1.amosar(nMostrar)+"**");
                        break;
                
                case 3: System.out.println("Indique el numero del mensaje que desea borrar: ");
                        int nBorrar=sc.nextInt();
                        b1.elimina(nBorrar);
                       
            }
        }while(opcion!=4);
    }
}
